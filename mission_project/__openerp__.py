{
"name" : "Mairie: Mission Project",
"category" : "crm",
"version" : "1.0",
"depends" : ["base", "crm_claim", "project", "project_issue_sheet","reclamation_mairie"],
"author" : "Me",
"description" : """\
Le service voirie de la mairie est amené à intervenir sur les lieux publics du secteur de la mairie
pour y effectuer des travaux publics ou à la suite d'une demande d'une personne. Le service
comprend un responsable, un adjoint et une équipe d'employés qui vont sur le terrain. Son activité
consiste à recueillir les demandes des citoyens ou plaignants qui viennent déposer une demande
d'intervention.
Ce Module permet la gestion des réclamations des citoyens ou plaignants en générant des missions et taches et
en affectant les employés ainsi que le matériel disponible pour intervenir dans cette réclamation.
""",
"data" : [
          
          "views/tache.xml",
          "data/donnee.sql",
          "data/tache_data.xml",
          
         
          
],

'update_xml': [
              
         
],
}
