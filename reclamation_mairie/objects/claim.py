# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
from operator import itemgetter
from openerp import tools
import time
from openerp.osv.fields import many2one
from openerp.tools.translate import _
from openerp import api
import datetime
from dateutil.relativedelta import relativedelta
from openerp import netsvc
import logging
_logger = logging.getLogger(__name__)

class abonnement(osv.osv):
    
    _inherit = "account.analytic.account"

    def _recurring_create_invoice(self, cr, uid, ids, automatic=False, context=None):
        context = context or {}
        invoice_ids = []
        current_date =  time.strftime('%Y-%m-%d')
        if ids:
            contract_ids = ids
        else:
            contract_ids = self.search(cr, uid, [('recurring_next_date','<=', current_date),('state','=', 'open'), ('recurring_invoices','=', True), ('type', '=', 'contract'), ('use_tasks','=',False)])
        if contract_ids:
            cr.execute('SELECT company_id, array_agg(id) as ids FROM account_analytic_account WHERE id IN %s GROUP BY company_id', (tuple(contract_ids),))
            for company_id, ids in cr.fetchall():
                for contract in self.browse(cr, uid, ids, context=dict(context, company_id=company_id, force_company=company_id)):
                    try:
                        invoice_values = self._prepare_invoice(cr, uid, contract, context=context)
                        invoice_ids.append(self.pool['account.invoice'].create(cr, uid, invoice_values, context=context))
                        next_date = datetime.datetime.strptime(contract.recurring_next_date or current_date, "%Y-%m-%d")
                        interval = contract.recurring_interval
                        if contract.recurring_rule_type == 'daily':
                            new_date = next_date+relativedelta(days=+interval)
                        elif contract.recurring_rule_type == 'weekly':
                            new_date = next_date+relativedelta(weeks=+interval)
                        elif contract.recurring_rule_type == 'monthly':
                            new_date = next_date+relativedelta(months=+interval)
                        else:
                            new_date = next_date+relativedelta(years=+interval)
                        self.write(cr, uid, [contract.id], {'recurring_next_date': new_date.strftime('%Y-%m-%d')}, context=context)
                        if automatic:
                            cr.commit()
                    except Exception:
                        if automatic:
                            cr.rollback()
                            _logger.exception('Fail to create recurring invoice for contract %s', contract.code)
                        else:
                            raise
        return invoice_ids
    
   
    
    

class Claim(osv.Model):
    _inherit = 'res.partner'
    _columns = {
              'cin' : fields.char('cin', size=64),
              'citoyen': fields.boolean('Citoyen', required=False),
              } 
       
    def name_get(self, cr, uid, ids, context=None):
        if not ids:
            return []
        result = []
        for line in self.browse(cr, uid, ids, context=context):
            if line.cin:
                result.append((line.id, (line.name + '-' + line.cin)))
            else:
                result.append((line.id, line.name))
        return result

class project(osv.osv):
    _inherit = "project.project"
    _columns = {
                'claim_id': fields.many2one('crm.claim', 'Réclamation'),
                }
    
class task(osv.osv):
    _inherit = "project.task"
    _columns = {
                'claim_id': fields.many2one('crm.claim', 'Réclamation'),
                }
    
class profileNomCin(osv.Model):
    _inherit = 'crm.claim'
    _track = {
        'state': {
            'crm_claim.cr_claim_approved': lambda self, cr, uid, obj, ctx = None: obj.state == 'validate',
            'crm_claim.cr_claim_refused': lambda self, cr, uid, obj, ctx = None: obj.state == 'refuse',
            'crm_claim.cr_claim_confirmed': lambda self, cr, uid, obj, ctx = None: obj.state == 'confirm',
            },
              }
    def _get_can_reset(self, cr, uid, ids, name, arg, context=None):
        """User can reset a claim request if he is an secretaire. """
        user = self.pool['res.users'].browse(cr, uid, uid, context=context)
        group_secretaire_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'reclamation_mairie', 'secretaire')[1]
        if group_secretaire_id in [g.id for g in user.groups_id]:
            return dict.fromkeys(ids, True)
        result = dict.fromkeys(ids, False)
        return result
    
    def _get_can_validate(self, cr, uid, ids, name, arg, context=None):
        """User can validate a claim request if he is a maire. """
        user = self.pool['res.users'].browse(cr, uid, uid, context=context)
        group_secretaire_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'reclamation_mairie', 'le_maire')[1]
        if group_secretaire_id in [g.id for g in user.groups_id]:
            return dict.fromkeys(ids, True)
        result = dict.fromkeys(ids, False)
        return result
    
    _columns = {
                'cin' : fields.char("Numéro d'identité", size=64),
                'nom' : fields.char('Nom', size=64),
#                 'project_id': fields.many2one('project.project', 'Mission'),
#                 'tasks': fields.one2many('project.task', 'claim_id', "Tâche(s)"),
                'can_reset': fields.function(_get_can_reset, type='boolean'),
                'can_validate': fields.function(_get_can_validate, type='boolean'),
                'state': fields.selection([('draft', 'To Submit'), 
                                           ('confirm', 'To Approve'),
                                           ('refuse', 'Refused'), 
                                           ('validate', 'Approved')],
                                          'Status', readonly=True, track_visibility='onchange', copy=False,
                                          help='The status is set to \'To Submit\', when a claim request is created.\
                                            \nThe status is \'To Approve\', when claim request is confirmed by secretary.\
                                            \nThe status is \'Refused\', when claim request is refused by manager or secretary.\
                                            \nThe status is \'Approved\', when claim request is approved by manager.'),
              }  
    _default = {
             'state': 'confirm',
             } 

    def unlink(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid, ids, context=context):
            if rec.state not in ['draft', 'confirm']:
                raise osv.except_osv(_('Warning!'),_('You cannot delete a claim which is in %s state.')%(rec.state))
        return super(profileNomCin, self).unlink(cr, uid, ids, context)
        
    def claim_reset(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'draft', })
#         to_unlink = []
#         for record in self.browse(cr, uid, ids, context=context):
#             for record2 in record.linked_request_ids:
#                 self.holidays_reset(cr, uid, [record2.id], context=context)
#                 to_unlink.append(record2.id)
#         if to_unlink:
#             self.unlink(cr, uid, to_unlink, context=context)
        return True
    
    def claim_confirm(self, cr, uid, ids, context=None):
#         for record in self.browse(cr, uid, ids, context=context):
#             if record.employee_id and record.employee_id.parent_id and record.employee_id.parent_id.user_id:
#                 self.message_subscribe_users(cr, uid, [record.id], user_ids=[record.employee_id.parent_id.user_id.id], context=context)
        return self.write(cr, uid, ids, {'state': 'confirm'})
    
    def claim_refuse(self, cr, uid, ids, context=None):
        obj_claim = self.pool.get('crm.claim')
        ids2 = obj_claim.search(cr, uid, [('user_id', '=', uid)])
        manager = ids2 and ids2[0] or False
        for claim in self.browse(cr, uid, ids, context=context):
                self.write(cr, uid, [claim.id], {'state': 'refuse', 'user_id': manager})
        return True    
    
    def claim_validate(self, cr, uid, ids, context=None):
        obj_claim = self.pool.get('crm.claim')
        ids2 = obj_claim.search(cr, uid, [('user_id', '=', uid)])
        manager = ids2 and ids2[0] or False
        self.write(cr, uid, ids, {'state':'validate'})
#         data_holiday = self.browse(cr, uid, ids)
#         for record in data_holiday:
#             if record.double_validation:
#                 self.write(cr, uid, [record.id], {'manager_id2': manager})
#             else:
#                 self.write(cr, uid, [record.id], {'manager_id': manager})
#             if record.holiday_type == 'employee' and record.type == 'remove':
#                 meeting_obj = self.pool.get('calendar.event')
#                 meeting_vals = {
#                     'name': record.name or _('Leave Request'),
#                     'categ_ids': record.holiday_status_id.categ_id and [(6,0,[record.holiday_status_id.categ_id.id])] or [],
#                     'duration': record.number_of_days_temp * 8,
#                     'description': record.notes,
#                     'user_id': record.user_id.id,
#                     'start': record.date_from,
#                     'stop': record.date_to,
#                     'allday': False,
#                     'state': 'open',            # to block that meeting date in the calendar
#                     'class': 'confidential'
#                 }   
#                 #Add the partner_id (if exist) as an attendee             
#                 if record.user_id and record.user_id.partner_id:
#                     meeting_vals['partner_ids'] = [(4,record.user_id.partner_id.id)]
#                     
#                 ctx_no_email = dict(context or {}, no_email=True)
#                 meeting_id = meeting_obj.create(cr, uid, meeting_vals, context=ctx_no_email)
#                 self._create_resource_leave(cr, uid, [record], context=context)
#                 self.write(cr, uid, ids, {'meeting_id': meeting_id})
#             elif record.holiday_type == 'category':
#                 emp_ids = obj_emp.search(cr, uid, [('category_ids', 'child_of', [record.category_id.id])])
#                 leave_ids = []
#                 for emp in obj_emp.browse(cr, uid, emp_ids):
#                     vals = {
#                         'name': record.name,
#                         'type': record.type,
#                         'holiday_type': 'employee',
#                         'holiday_status_id': record.holiday_status_id.id,
#                         'date_from': record.date_from,
#                         'date_to': record.date_to,
#                         'notes': record.notes,
#                         'number_of_days_temp': record.number_of_days_temp,
#                         'parent_id': record.id,
#                         'employee_id': emp.id
#                     }
#                     leave_ids.append(self.create(cr, uid, vals, context=None))
#                 for leave_id in leave_ids:
#                     # TODO is it necessary to interleave the calls?
#                     for sig in ('confirm', 'validate', 'second_validate'):
#                         self.signal_workflow(cr, uid, [leave_id], sig)
        return True
   
    
    def onchange_partner_id(self, cr, uid, ids, partner_id, email=False, context=None):
        result = super(profileNomCin, self).onchange_partner_id(cr, uid, ids, partner_id, email=False, context=None)
        if partner_id:
               partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
               result['value'].update({'cin': partner.cin , 'nom': partner.name})
        return result
        
    def action_crm_claim2mission(self, cr, uid, ids, context=None):
        if ids:
            claim_obj = self.pool.get('crm.claim')
            project_pool = self.pool.get('project.project')
            task_obj = self.pool.get('project.task')
            for claim in claim_obj.browse(cr, uid, ids, context=context):
                project_values = {
                                   'name': claim.name,
                                   'claim_id': claim.id,
                                   }
                project = project_pool.create(cr, uid, project_values, context=context)
                
                task_values = {
                                   'name': claim.name,
                                   'project_id':project,
                                   'claim_id': claim.id,
                                   }
                task = task_obj.create(cr, uid, task_values, context=context)
            
            return project
    
class Stage(osv.Model):
    _inherit = 'crm.claim.stage'
    _columns = {
                'code' : fields.char('code'),
              }  
class crm_claim2tache(osv.osv_memory):
    _name = "crm.claim2tache"
    
    _columns = {
                'claim_id':fields.many2one('crm.claim', 'Reclamation'),
                'project_id': fields.many2one('project.project', 'Mission'),
                }
    
    def crm_claim2tache(self, cr, user, ids, context={}):
        wiz = self.browse(cr, user, ids[0], context)
        task_obj = self.pool.get('project.task')
        project = wiz.project_id
        claim = wiz.claim_id
        if(project):
            project.claim_id = claim.id
        task_values = {
                  'name': claim.name,
                  'project_id':project.id ,
                  'claim_id': claim.id,
                                   }
        task = task_obj.create(cr, user, task_values, context=context)
        return claim   


class crm_claim_report(osv.Model):
    _inherit = 'crm.claim.report'
    _columns = {
                'code' : fields.char('Code'),
              }
    def init(self, cr):
 
        """ Display Number of cases And Section Name
        @param cr: the current row, from the database cursor,
         """
 
        tools.drop_view_if_exists(cr, 'crm_claim_report')
        cr.execute("""
            create or replace view crm_claim_report as (
              select
                    min(c.id) as id,
                    c.date as claim_date,
                    c.date_closed as date_closed,
                    c.date_deadline as date_deadline,
                    c.user_id,
                    c.stage_id,
                    a.code as code,
                    c.section_id,
                    c.partner_id,
                    c.company_id,
                    c.categ_id,
                    c.name as subject,
                    count(*) as nbr,
                    c.priority as priority,
                    c.type_action as type_action,
                    c.create_date as create_date,
                    avg(extract('epoch' from (c.date_closed-c.create_date)))/(3600*24) as  delay_close,
                    (SELECT count(id) FROM mail_message WHERE model='crm.claim' AND res_id=c.id) AS email,
                    extract('epoch' from (c.date_deadline - c.date_closed))/(3600*24) as  delay_expected
                from
                    crm_claim c
                    inner join crm_claim_stage a on c.stage_id=a.id
                group by c.date,\
                        c.user_id,c.section_id, c.stage_id,a.code,\
                        c.categ_id,c.partner_id,c.company_id,c.create_date,
                        c.priority,c.type_action,c.date_deadline,c.date_closed,c.id
            )""")

    
    
    

     
      
    
    
