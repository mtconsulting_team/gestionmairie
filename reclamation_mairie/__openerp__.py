{
"name" : "Mairie: Gestion Reclamation",
"category" : "crm",
"version" : "1.0",
"depends" : ["base", "crm_claim","project","project_issue_sheet","document",'gestion_mairie'],
"author" : "Me",
"description" : """\
Le service voirie de la mairie est amené à intervenir sur les lieux publics du secteur de la mairie
pour y effectuer des travaux publics ou à la suite d'une demande d'une personne. Le service
comprend un responsable, un adjoint et une équipe d'employés qui vont sur le terrain. Son activité
consiste à recueillir les demandes des citoyens ou plaignants qui viennent déposer une demande
d'intervention.
Ce Module permet la gestion des réclamations des citoyens ou plaignants en générant des missions et taches et
en affectant les employés ainsi que le matériel disponible pour intervenir dans cette réclamation.
""",
"data" : [
          "data/groups.xml",
          "data/crm_claim_data.xml",
          "views/Claims.xml",
          "views/mairie_workflow.xml",
          
],

'update_xml': [
              
         
],
}
