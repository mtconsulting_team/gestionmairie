# -*- coding: utf-8 -*-
from openerp.osv import osv
from openerp.osv import fields, osv
from openerp import netsvc
from datetime import datetime
import datetime
from dateutil.relativedelta import relativedelta

class paiement_anticipe(osv.osv_memory):

    _name = "paiement.anticipe"
    
    def default_get(self, cr, uid, fields, context=None):
        res = super(paiement_anticipe, self).default_get(cr, uid, fields, context)
        actives = context.get('partner_id', []) 
        
        if actives:
            partner = self.pool.get('res.partner').browse(cr, uid, actives, context=context)
            res.update({'partner_id': partner.id})
          
        return res
    
    def _get_date_invoice(self, cr, user, ids, context={}):
        wiz = self.browse(cr, user, ids[0], context)
        date = datetime.now()
        newdate = date.replace(month=11)
        return newdate
        
    def paiement_anticipe(self, cr, user, ids, context={}):
        wiz = self.browse(cr, user, ids[0], context)
        partner = wiz.partner_id
        abonnement=wiz.abonnement_id
        period_debut=wiz.periode_debut
        periode_fin=wiz.periode_fin
        type = 'Abonnement'
#         date = datetime.now()
#         date = date.replace(day=1)
        ##########Ajouter par Rahma
        obj = self.pool.get('account.analytic.account')
        ids = obj.search(cr, user, [('name','=',abonnement.name),('state', '=', 'open'), ('amount_max', '!=', 0), ('partner_id', '=', partner.id)])
        invoice_line_obj = self.pool.get('account.invoice.line')
        invoice_line_ids=invoice_line_obj.search(cr, user, [('account_analytic_id','=',abonnement.id),('invoice_id.period_id','=',period_debut.id)])
        if(not invoice_line_ids):
                obj.recurring_create_invoice_pay_anticipe(cr, user, ids, period_debut , context)
        next_date = datetime.datetime.strptime(period_debut.date_start, "%Y-%m-%d")
        date_fin = datetime.datetime.strptime(periode_fin.date_start, "%Y-%m-%d")
        while (next_date!=date_fin):
            next_date = next_date+relativedelta(months=+1)
            obj_period = self.pool.get('account.period')
            ids_period = obj_period.search(cr, user, [('date_start','=',next_date)])
            period=obj_period.browse(cr, user, ids_period, context=context)
            invoice_line_ids=invoice_line_obj.search(cr, user, [('account_analytic_id','=',abonnement.id),('invoice_id.period_id','=',period.id)])
            if(not  invoice_line_ids):
                    obj.recurring_create_invoice_pay_anticipe(cr, user, ids, period , context)
        
        ##########Fin par Rahma
#         dt = datetime.now() 
#         if dt.month < 10 :
#             period = str(0) + str(dt.month) + "/" + str(dt.year)
#         else:
#             period = str(dt.month) + "/" + str(dt.year)
#         period_id = self.pool.get("account.period").search(cr, user, [('name', '=', period)])[0] 
#         
#         mois = wiz.mois  
#         if mois:
#             if mois == 'janvier':
#                date_inv = date.replace(month=1)
#             elif mois == 'fevrier':
#                date_inv = date.replace(month=2)
#             elif mois == 'mars':
#                date_inv = date.replace(month=3)
#             elif mois == 'avril':
#                date_inv = date.replace(month=4)
#             elif mois == 'mai':
#                date_inv = date.replace(month=5)
#             elif mois == 'juin':
#                date_inv = date.replace(month=6)
#             elif mois == 'juillet':
#                date_inv = date.replace(month=7)
#             elif mois == 'aout':
#                date_inv = date.replace(month=8)
#             elif mois == 'septembre':
#                date_inv = date.replace(month=9)
#             elif mois == 'octobre':
#                date_inv = date.replace(month=10)
#             elif mois == 'novembre':
#                date_inv = date.replace(month=11)
#             elif mois == 'decembre':
#                date_inv = date.replace(month=12)
#             
#         # liste des membres clients
#         type_impot = self.pool.get("account.journal").search(cr, user, [('name', '=', type)]) 
#         impot_ids = self.pool.get("gm.impot").search(cr, user, [('periodicite.name', '=', "Abonnement"), ('partner_id', '=', partner.id)]) 
#         ids = []
#         clients_ids = []
#          # boucle pour parcourir les clients
#         for impot in self.pool.get("gm.impot").browse(cr, user, impot_ids, context):
#             clients_ids.append(partner.id)
#             invoice_obj = self.pool.get('account.invoice')
#              # recuperer le compte client
#             account_id = partner.property_account_receivable and partner.property_account_receivable.id or False            
#             payment_term = impot.periodicite.property_payment_term and impot.periodicite.property_payment_term.id or False
#             
#             if True:            
#                     invoice_id = invoice_obj.create(cr, user, {
#                        'partner_id': partner.id,
#                        'account_id': partner.property_account_receivable and partner.property_account_receivable.id or False  ,
#                        'payment_term': payment_term,
#                      #  'credit':partner.payment_amount_overdue,
#                        'journal_id':impot.periodicite.id,
#                        'date_invoice':date_inv,
#                        'period_id': period_id,
#                        'fiscal_position': False
#                            }, context={}) 
#                                  
#                     invoice_line_obj = self.pool.get('account.invoice.line')
#                     amount = impot.patente                   
#                     quantity = 1
#                     line_value = {                           
#                         'name':type
#                         }
#             # Recuperer les valeurs des champs calculer en fonction des produits, clients ...
#                     line_dict = invoice_line_obj.product_id_change(cr, user, {},
#                             False, False, quantity, '', 'out_invoice', partner.id, False, price_unit=amount, context=context)
#                 
#                     line_value.update(line_dict['value'])
#                     line_value['price_unit'] = amount
#                   #  if impot.periodicite and impot.periodicite.taxe_id:
#                   #      idx = impot.periodicite.taxe_id.id
#                    #     line_value['invoice_line_tax_id'] = [(6, 0, [idx])]
#                        
#                     line_value['invoice_id'] = invoice_id
#                     ids.append(invoice_id)
#              # creation  de la ligne
#                     invoice_line_id = invoice_line_obj.create(cr, user, line_value, context=context)
#         
#                     invoice_obj.write(cr, user, invoice_id, {'invoice_line': [(6, 0, [invoice_line_id])]}, context=context)
#                     netsvc.LocalService('workflow').trg_validate(user, 'account.invoice', invoice_id, 'invoice_open', cr)
#                   #  self.pool.get("gm.impot").write(cr, user, impot.id, {'existe_dec':True})
#                         
#         self.pool.get('res.partner').write(cr, user, clients_ids, {})
#         
        
       
        return {'type': 'ir.actions.act_window_close'}
    
    _columns = {
       
        'partner_id': fields.many2one('res.partner', 'Partner', readonly=True, required=True),
        'abonnement_id': fields.many2one('account.analytic.account','Abonnement', required=True),
        'date_invoice': fields.function(_get_date_invoice, type='date', string='date_invoice'),
        'periode_debut': fields.many2one('account.period','Période début'),
        'periode_fin': fields.many2one('account.period','Période fin'),
        
                }

paiement_anticipe()
