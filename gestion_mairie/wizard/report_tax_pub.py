# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
from openerp import netsvc
from datetime import datetime

####################################################"
##################Rapport des tax_pubs##############
####################################################"
class report_tax_pub(osv.osv_memory):
    _name = "report.tax.pub"
    _description = "Rapport des taxes sur publicites"
    _columns = {
        'date_begin': fields.date('Date inferieur', required=True),
        'date_end': fields.date('Date superieur', required=True),
        'abn_payes':fields.boolean("Les taxes sur publicites payees"),
        'periodicite':fields.many2one('account.journal', 'Type impot', required=True),
                }
    
    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = {}
        
        data['form'] = self.read(cr, uid, ids, ['periodicite', 'date_begin', 'date_end','abn_payes'], context=context)[0]
        for field in ['periodicite', 'date_begin', 'date_end','abn_payes']:
            if isinstance(data['form'][field], tuple):
                data['form'][field] = data['form'][field][0]
        return self._print_report(cr, uid, ids, data, context=context)

    def _print_report(self, cr, uid, ids, data, context=None):
       if(data['form']['abn_payes']):
           return self.pool['report'].get_action(cr, uid, [], 'gestion_mairie.rapport_tax_pub_paye', data=data, context=context)
       else:
           return self.pool['report'].get_action(cr, uid, [], 'gestion_mairie.rapport_tax_pub_impaye', data=data, context=context)
   

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
