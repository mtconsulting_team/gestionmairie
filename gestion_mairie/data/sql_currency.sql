update res_currency set description='francs' where name='XOF';
update res_currency set rounding=50 where name='XOF';
update decimal_precision set digits=0 where name='Account';
update decimal_precision set digits=0 where name='Payment Term';
update res_lang set grouping='[3,3,3]';