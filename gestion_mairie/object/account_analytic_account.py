# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
from operator import itemgetter
import time
from openerp.osv.fields import many2one
from openerp.tools.translate import _
from openerp import api
import datetime
from datetime import datetime
from dateutil.relativedelta import relativedelta
from openerp import netsvc
import logging
_logger = logging.getLogger(__name__)

# class testid(osv.osv):
#     _name = 'test.test'
#     _desciption = 'Test'
#     _columns = {
#         'name': fields.char('Test', size=64),
#     }

class abonnement(osv.osv):
    
    _inherit = "account.analytic.account"

    def on_change_partner_id(self, cr, uid, ids, partner_id, name, context=None):
        res = {}
        if partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            res['registre_commerce'] = partner.ninea
            res['numero_cantine'] = partner.numero_cantine
            res['surface_occupee'] = partner.surface_occupee
           # res['zone_residence'] = partner.zone_residence
# MOI
            res['agent_traitant'] = partner.user_id.name
            #Rahma res['lieu_implantation'] = partner.lieu_implantation_id.name
            res['domaine_activite'] = partner.domaine_activite_id
            res['lieu_residence'] = partner.lieu_residence
            res['zone_residence'] = partner.zone_residence
#Fin
            if partner.user_id:
                res['manager_id'] = partner.user_id.id
            if partner.country_id: 
                res['country_id']= partner.country_id.id
            res['street']= partner.street
            res['street2']= partner.street2
            res['phone']= partner.phone
            res['mobile']= partner.mobile
            res['email']= partner.email
            if not name:
                    res['name'] = _('Abonnement: ') + partner.name
        return {'value': res}
        
    def onchange_lieu_residence(self, cr, uid, ids, lieu_residence, context=None):
        res = {}
        zone_obj = self.pool.get('mairie.lieuresidence')
        zone = zone_obj.browse(cr, uid, lieu_residence, context=context)
        if lieu_residence:
            res['zone_residence'] = zone.zone_residence
             #    return {'value':{'zone_residence': zone.zone_residence.id}}
            return {'value': res}
        return {}

    def on_change_partner_id_patente(self, cr, uid, ids, partner_id, name, context=None):
        res = {}
        if partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            if partner.user_id:
                res['manager_id'] = partner.user_id.id
            if partner.country_id: 
                res['country_id']= partner.country_id.id
            res['street']= partner.street
            res['street2']= partner.street2
            res['phone']= partner.phone
            res['mobile']= partner.mobile
            res['email']= partner.email
            res['zone_residence'] = partner.zone_residence
            res['lieu_residence'] = partner.lieu_residence
            if not name:
                    res['name'] = _('Patente: ') + partner.name
        return {'value': res}

    def on_change_partner_id_tax_pub(self, cr, uid, ids, partner_id, name, context=None):
        res = {}
        if partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            if partner.user_id:
                res['manager_id'] = partner.user_id.id
            if partner.country_id: 
                res['country_id']= partner.country_id.id
            res['street']= partner.street
            res['street2']= partner.street2
            res['phone']= partner.phone
            res['mobile']= partner.mobile
            res['email']= partner.email
            res['zone_residence'] = partner.zone_residence
            res['lieu_residence'] = partner.lieu_residence
            if not name:
                    res['name'] = _('Taxe sur publicite: ') + partner.name
        return {'value': res}
    


        
    def _recurring_create_invoice_pay_anticipe(self, cr, uid, ids,  period_debut ,automatic=False, context=None):
        context = context or {}
        invoice_ids = []
        current_date =  time.strftime('%Y-%m-%d')
        period_debut_date=period_debut.date_start
        if ids:
            contract_ids = ids
        else:
            contract_ids = self.search(cr, uid, [('recurring_next_date','<=', current_date), ('state','=', 'open'), ('recurring_rule_type','=','monthly'), ('recurring_invoices','=', True), ('type', '=', 'contract')])
        if contract_ids:
            cr.execute('SELECT company_id, array_agg(id) as ids FROM account_analytic_account WHERE id IN %s GROUP BY company_id', (tuple(contract_ids),))
            for company_id, ids in cr.fetchall():
                for contract in self.browse(cr, uid, ids, context=dict(context, company_id=company_id, force_company=company_id)):
                    try:
                        invoice_values = self._prepare_invoice(cr, uid, contract, context=context)
                        invoice_ids.append(self.pool['account.invoice'].create(cr, uid, invoice_values, context=context))
                        next_date = datetime.strptime(period_debut_date, "%Y-%m-%d")
                        interval = contract.recurring_interval
                        if contract.recurring_rule_type == 'daily':
                            new_date = next_date+relativedelta(days=+interval)
                        elif contract.recurring_rule_type == 'weekly':
                            new_date = next_date+relativedelta(weeks=+interval)
                        elif contract.recurring_rule_type == 'monthly':
                            new_date = next_date+relativedelta(months=+interval)
                        else:
                            new_date = next_date+relativedelta(years=+interval)
                        self.write(cr, uid, [contract.id], {'recurring_next_date': new_date.strftime('%Y-%m-%d')}, context=context)
                        if automatic:
                            cr.commit()
                    except Exception:
                        if automatic:
                            cr.rollback()
                            _logger.exception('Fail to create recurring invoice for contract %s', contract.code)
                        else:
                            raise
        return invoice_ids

#     def recurring_create_invoice_pay_anticipe(self, cr, uid, ids,  period_debut , context=None):
#         return self._recurring_create_invoice_pay_anticipe(cr, uid, ids,  period_debut , context=context)
    
    def recurring_create_invoice_pay_anticipe(self, cr, uid, ids,  period_debut , context=None):      
        invoice_ids = self._recurring_create_invoice_pay_anticipe(cr, uid, ids,  period_debut , context=context) 
        for invoice_id in invoice_ids:
            netsvc.LocalService('workflow').trg_validate(uid, 'account.invoice', invoice_id, 'invoice_open', cr)
        return invoice_ids


    def _prepare_invoice_lines(self, cr, uid, contract, fiscal_position_id, context=None):
        fpos_obj = self.pool.get('account.fiscal.position')
        fiscal_position = None
        if fiscal_position_id:
            fiscal_position = fpos_obj.browse(cr, uid,  fiscal_position_id, context=context)
        invoice_lines = []
        for line in contract.recurring_invoice_line_ids:

            res = line.product_id
            account_id = res.property_account_income.id
            if not account_id:
                account_id = res.categ_id.property_account_income_categ.id
            account_id = fpos_obj.map_account(cr, uid, fiscal_position, account_id)

            taxes = res.taxes_id or False
            tax_id = fpos_obj.map_tax(cr, uid, fiscal_position, taxes)

            invoice_lines.append((0, 0, {
                'name': line.name,
                'account_id': account_id,
                'account_analytic_id': contract.id,
                'price_unit': line.price_unit or 0.0,
                'quantity': line.quantity,
                'uos_id': line.uom_id.id or False,
                'product_id': line.product_id.id or False,
                'invoice_line_tax_id': [(6, 0, tax_id)],
            }))
        return invoice_lines
            
    def _prepare_invoice_lines(self, cr, uid, contract, invoice_id, context=None):
        invoice_lines = []
        invoice_line_obj = self.pool.get('account.invoice.line')
        if not contract.amount_max:
            raise osv.except_osv(_(u'erreur'), _(u'Le montant est null'))
        amount = contract.amount_max
        quantity = 1
        if contract.recurring_rule_type=='monthly':
            line_value = {                           
                'name':'Abonnement'
              
             }
        elif (contract.is_patente==True):
             line_value = {                           
                'name':'Patente'
              
             }
        else:
            line_value = {                           
                'name':'Taxe sur publicité'
              
             }
             
            # Recuperer les valeurs des champs calculer en fonction des produits, clients ...
        line_dict = invoice_line_obj.product_id_change(cr, uid, {},
                            False, False, quantity, '', 'out_invoice', contract.partner_id.id, False, price_unit=amount, context=context)
                
        line_value.update(line_dict['value'])
        line_value['price_unit'] = amount
        line_value['account_analytic_id'] = contract.id
        invoice_lines.append((0, 0, line_value))
        
       #################################
       # line_value['invoice_id'] = invoice_id
        invoice_line_id = invoice_line_obj.create(cr, uid, line_value, context=context)
        self.pool.get('account.invoice').write(cr, uid, invoice_id, {'invoice_line': [(6, 0, [invoice_line_id])]}, context=context)
        #netsvc.LocalService('workflow').trg_validate(uid, 'account.invoice', invoice_id, 'invoice_open', cr)
        ##################
        return invoice_lines

    def _prepare_invoice_data(self, cr, uid, contract, context=None):
        context = context or {}

        journal_obj = self.pool.get('account.journal')

        if not contract.partner_id:
            raise osv.except_osv(_('No Customer Defined!'),_("You must first select a Customer for Contract %s!") % contract.name )

        fpos = contract.partner_id.property_account_position or False
        if contract.recurring_rule_type=='monthly':
            journal_ids = journal_obj.search(cr, uid, [('code', '=','ABO')], limit=1)
        elif (contract.is_patente==True):
            journal_ids = journal_obj.search(cr, uid, [('code', '=','PT')], limit=1)
        else:
            journal_ids = journal_obj.search(cr, uid, [('code', '=','TAXPU')], limit=1)

        if not journal_ids:
            raise osv.except_osv(_('Error!'),
            _('Please define a sale journal for the company "%s".') % (contract.company_id.name or '', ))

        partner_payment_term = contract.partner_id.property_payment_term and contract.partner_id.property_payment_term.id or False

        currency_id = False
        if contract.pricelist_id:
            currency_id = contract.pricelist_id.currency_id.id
        elif contract.partner_id.property_product_pricelist:
            currency_id = contract.partner_id.property_product_pricelist.currency_id.id
        elif contract.company_id:
            currency_id = contract.company_id.currency_id.id

        invoice = {
           'account_id': contract.partner_id.property_account_receivable.id,
           'type': 'out_invoice',
           'partner_id': contract.partner_id.id,
           'lieu_residence': contract.partner_id.lieu_residence.id,
           'zone_residence':contract.partner_id.zone_residence.id,
           'currency_id': currency_id,
           'journal_id': len(journal_ids) and journal_ids[0] or False,
           'date_invoice': contract.recurring_next_date,
           'origin': contract.code,
           'fiscal_position': fpos and fpos.id,
           'payment_term': partner_payment_term,
           'state': 'open',
           'credit': contract.amount_max,
           'company_id': contract.company_id.id or False,
        }
        return invoice        
    
    def _prepare_invoice(self, cr, uid, contract, context=None):
        invoice = self._prepare_invoice_data(cr, uid, contract, context=context)
        invoice['invoice_line'] = self._prepare_invoice_lines(cr, uid, contract, invoice['fiscal_position'], context=context)
        
        return invoice

    def _recurring_create_invoice(self, cr, uid, ids, automatic=False, context=None):
        context = context or {}
        invoice_ids = []
        current_date =  time.strftime('%Y-%m-%d')
        if ids:
            contract_ids = ids
        else:
            contract_ids = self.search(cr, uid, [('recurring_next_date','<=', current_date),('state','=', 'open'), ('recurring_invoices','=', True), ('type', '=', 'contract')])
        if contract_ids:
            cr.execute('SELECT company_id, array_agg(id) as ids FROM account_analytic_account WHERE id IN %s GROUP BY company_id', (tuple(contract_ids),))
            for company_id, ids in cr.fetchall():
                for contract in self.browse(cr, uid, ids, context=dict(context, company_id=company_id, force_company=company_id)):
                    try:
                        invoice_values = self._prepare_invoice(cr, uid, contract, context=context)
                        invoice_ids.append(self.pool['account.invoice'].create(cr, uid, invoice_values, context=context))
                        next_date = datetime.strptime(contract.recurring_next_date or current_date, "%Y-%m-%d")
                        interval = contract.recurring_interval
                        if contract.recurring_rule_type == 'daily':
                            new_date = next_date+relativedelta(days=+interval)
                        elif contract.recurring_rule_type == 'weekly':
                            new_date = next_date+relativedelta(weeks=+interval)
                        elif contract.recurring_rule_type == 'monthly':
                            new_date = next_date+relativedelta(months=+interval)
                        else:
                            new_date = next_date+relativedelta(years=+interval)
                        self.write(cr, uid, [contract.id], {'recurring_next_date': new_date.strftime('%Y-%m-%d')}, context=context)
                        if automatic:
                            cr.commit()
                    except Exception:
                        if automatic:
                            cr.rollback()
                            _logger.exception('Fail to create recurring invoice for contract %s', contract.code)
                        else:
                            raise
        return invoice_ids
 
    def recurring_create_invoice(self, cr, uid, ids, context=None): 
        #super(abonnement, self).     
        invoice_ids = self._recurring_create_invoice(cr, uid, ids, automatic=True, context=context) 
        for invoice_id in invoice_ids:
           netsvc.LocalService('workflow').trg_validate(uid, 'account.invoice', invoice_id, 'invoice_open', cr)
        return invoice_ids
    
    
    def _cron_recurring_create_invoice(self, cr, uid, context=None):
        return self.recurring_create_invoice(cr, uid, [], context=context)

    _columns = {
        'numero_cantine':  fields.char('Numéro cantine'),
        'registre_commerce': fields.char('Numéro registre de commerce'),
        'surface_occupee':  fields.char('Surface occupée (en m2)'),
        'country_id': fields.many2one('res.country', 'Country'),
        'street': fields.char('Adresse professionnelle'),
        'street2': fields.char('Street2'),
        'phone': fields.char('Téléphone fixe'),
        'mobile': fields.char('Téléphone portable'),
        'email': fields.char('Email'),
        'is_patente': fields.boolean("C'est une patente"),# True if patente and False if tax sur publicité
        'taxe_publicitaire': fields.float('Panneau publicitaire'),
# MOI
        'agent_traitant': fields.char('Agent traitant'),
        #'domaine_activite': fields.char('Domaine d\'activité'),
        'domaine_activite': fields.many2one('mairie.domaine', 'Domaine d\'activité'),
        #'lieu_implantation': fields.char('Lieu d\'implantation', size=32),
        #'zone_residence': fields.char('Zone de résidence', size=32),
        #'lieu_residence': fields.char('Lieu de residence', size=32),
        #'lieu_residence_id': fields.many2one('test.test', 'Lieu de résidence'),
        'lieu_residence': fields.many2one('mairie.lieuresidence', 'Lieu de résidence'),
        'zone_residence': fields.many2one('mairie.zoneresidence', 'Zone de résidence'),
        
    }
    
    def _calcul_date(self, cr, uid, context=None):
        res={} 
        jour=1
        mois=1
        date_recurring_next=time.strftime('%Y-%m-%d')
        if(context.has_key('default_recurring_rule_type')):
            if ( context['default_recurring_rule_type']=='yearly'):
                if(context['default_is_patente']):
                    journal_id=self.pool.get('account.journal').search(cr, uid, [('code', '=','PT')], limit=1)
                else:
                    journal_id=self.pool.get('account.journal').search(cr, uid, [('code', '=','TAXPU')], limit=1)
                journal=self.pool.get('account.journal').browse(cr, uid, journal_id, context=context)
                if(journal.jour and journal.mois):
                    jour=int(journal.jour) 
                    mois=int(journal.mois) 
                    date_recurring_next= datetime.now().today().replace(month=mois, day=jour)
#        print(date_recurring_next)
        return date_recurring_next
        
    _defaults = {
                'fix_price_invoices': True,
                'recurring_invoices': True,
                'recurring_next_date': _calcul_date, 
        
        }
    